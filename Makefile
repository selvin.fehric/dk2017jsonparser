jsonparser: lex.yy.c 
	g++ -std=c++11 main.cpp parser.cpp -lfl -o json-parser
lex.yy.c:
	flex scanner.l
clean:
	rm lex.yy.c json-parser
