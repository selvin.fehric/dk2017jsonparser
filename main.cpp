#include <iostream>
#include <vector>

#include "token.hpp"
#include "parser.h"
#include "lex.yy.c"

using namespace std;
int main(void)
{
  vector<Token> tokens;
  int yytoken;
  while( yytoken = yylex() ){
    tokens.push_back(Token(yytoken, yytext));
    cout << yytext << endl;	
  }
  Parser p(tokens);
  if(p.parse())
    cout << "Uneseni string je ispravan." << endl;
  else 
    cout << "Uneseni string nije ispravan." << endl;
  return 0;
}
