#include <iostream>
#include <string>
#include <vector>
#include "token.hpp"
#include "parser.h"

using namespace std;
/*
 * Gramatika iz fajla json.txt
*/

Parser::Parser(std::vector<Token> const & t) 
  : tokens(t), bufferLength(t.size())
{}

bool Parser::parse(){
  currentIndex = 0;
  bool retval = Start();

  if(currentIndex < bufferLength) retval = false;
  
  return retval;
}

bool Parser::epsilon(){
cout << currentIndex << " epsilon" << endl;
  return true;  
}

bool Parser::terminal(int t){
  if(currentIndex >= bufferLength) return false;
  bool x=  (t == tokens[currentIndex].tag);
  if (x == true)
   currentIndex++;
  return x;
}

bool Parser::Start(){
  int save = currentIndex;
  cout << currentIndex << " start" << endl;
  return terminal(OPENBRACES) && pairs() && terminal(CLOSEDBRACES);
}

bool Parser::pairs(){
int save = currentIndex;
  cout << currentIndex << " pairs" << endl;
  return (pair() && pairs_tail()) || epsilon();
}

bool Parser::pair(){
int save = currentIndex;
  cout << currentIndex << " pair" << endl;
  return terminal(STRING) && terminal(COLON) && value();
}

bool Parser::pairs_tail(){
int save = currentIndex;
  cout << currentIndex << " pairs tail" << endl;
  return (terminal(COMMA) && pairs()) || epsilon();
}

bool Parser::value(){
  int save = currentIndex;
  cout << currentIndex << " value" << endl;
  return terminal(STRING) || terminal(NUMBER) || terminal(TRUE) || terminal(FALSE) || terminal(NULLABLE) || Start() || array();
}

bool Parser::array(){
  return terminal(OPENBRACKET) && elements() && terminal(CLOSEDBRACKET);
}

bool Parser::elements(){
  return (value() && elements_tail()) || epsilon();
}
bool Parser::elements_tail(){
  return (terminal(COMMA) && elements()) || epsilon();
}


